import java.util.*;

/** Quaternions. Basic operations. */
public class Quaternion {
   /*Lahenduseks leidsin motivatsiooni enos lingilt kus oli ära seletatud mis funktsioon tegema peab.
   Osade funktsioonide kohta vaatasin lahendust veebist. A`la kuidas HashCode ülekirjutatkse.
   Peamise arusaama kuidas testid ja kogu ülesanne toimub sain siit:
   https://introcs.cs.princeton.edu/java/32class/Quaternion.java.html. Kuid otseselt koodi seal maha ei kirjutanud.*/

   public static final double DELTA = 0.000001;
   private double a;
   private double b;
   private double c;
   private double d;

   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion (double a, double b, double c, double d) {
      this.a = a;
      this.b = b;
      this.c = c;
      this.d = d;
   }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() { return this.a; }

   /** Imaginary part i of the quaternion. 
    * @return imaginary part i
    */
   public double getIpart() { return this.b; }

   /** Imaginary part j of the quaternion. 
    * @return imaginary part j
    */
   public double getJpart() { return this.c; }

   /** Imaginary part k of the quaternion. 
    * @return imaginary part k
    */
   public double getKpart() { return this.d; }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion: 
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
      String returnStr = "";
      returnStr += a;
      if (b < 0){
         returnStr += b + "i";
      }else if (Math.abs(b) < DELTA){
         returnStr += "";
      }else{
         returnStr += "+" + b + "i";
      }
      if (c < 0){
         returnStr += c + "j";
      }else if (Math.abs(c) < DELTA){
         returnStr += "";
      }else{
         returnStr += "+" + c + "j";
      }
      if (d < 0){
         returnStr += d + "k";
      }else if (Math.abs(d) < DELTA){
         returnStr += "";
      }else{
         returnStr += "+" + d + "k";
      }
      return returnStr;

   }

   /** Conversion from the string to the quaternion. 
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent 
    *     a quaternion (defined by the <code>toString</code> method)
    * @param f string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
   public static Quaternion valueOf (String f) {
      try {
         String s = normalize(f);
         char[] charArr = s.toCharArray();
         List<String> parts = new ArrayList<>();
         String[] splited = s.split("\\+|-");
         if (splited[0].equals("") && splited.length == 5) {
            splited = Arrays.copyOfRange(splited, 1, splited.length);
         }

         if (splited.length > 4) {
            throw new RuntimeException("Invalid input string: " + f);
         }
         for (int n = 0; n < 4; n++) {
            if (charArr[0] == '-' || charArr[0] == '+') {
               if (n == 0) {
                  parts.add(charArr[0] + splited[n]);
               } else {
                  parts.add(charArr[0] + splited[n].substring(0, splited[n].length() - 1));
               }
               charArr = Arrays.copyOfRange(charArr, splited[n].length() + 1, charArr.length);
            } else {
               if (n == 0) {
                  parts.add(splited[n]);
               } else {
                  parts.add(splited[n].substring(0, splited[n].length() - 1));
               }
               charArr = Arrays.copyOfRange(charArr, splited[n].length(), charArr.length);
            }
         }
         return new Quaternion(Double.parseDouble(parts.get(0)),
                 Double.parseDouble(parts.get(1)),
                 Double.parseDouble(parts.get(2)),
                 Double.parseDouble(parts.get(3)));
      }catch (Exception e){
         throw new RuntimeException("Invalid input string: " + f);
      }
   }
   public static String normalize(String s){
      if (s.contains("i") && s.contains("j") && s.contains("k")){
         return s;
      }
      if (!s.contains("i")){
         return s + "+0i+0j+0k";
      }
      if (!s.contains("j")){
         return s + "+0j+0k";
      }
      if (!s.contains("k")){
         return s + "+0k";
      }
      return "";
   }
   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Quaternion(getRpart(), getIpart(), getJpart(), getKpart());
   }

   /** Test whether the quaternion is zero. 
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {

      return Math.abs(a) < DELTA && Math.abs(b) < DELTA && Math.abs(c) < DELTA && Math.abs(d) < DELTA;
   }

   /** Conjugate of the quaternion. Expressed by the formula 
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {
      return new Quaternion(getRpart(), -getIpart(), -getJpart(), -getKpart());
   }

   /** Opposite of the quaternion. Expressed by the formula 
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {
      return new Quaternion(-getRpart(), -getIpart(), -getJpart(), -getKpart());
   }

   /** Sum of quaternions. Expressed by the formula 
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {
      return new Quaternion(getRpart() + q.getRpart(), getIpart() + q.getIpart(),
              getJpart() + q.getJpart(), getKpart() + q.getKpart());
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {
      double newRealPart = this.a*q.getRpart() - this.b*q.getIpart() - this.c*q.getJpart() - this.d*q.getKpart();
      double newIPart = this.a*q.getIpart() + this.b*q.getRpart() + this.c*q.getKpart() - this.d*q.getJpart();
      double newJPart = this.a*q.getJpart() - this.b*q.getKpart() + this.c*q.getRpart() + this.d*q.getIpart();
      double newKPart = this.a*q.getKpart() + this.b*q.getJpart() - this.c*q.getIpart() + this.d*q.getRpart();
      return new Quaternion(newRealPart, newIPart, newJPart, newKPart);
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {
      return new Quaternion(this.a * r, this.b * r, this.c * r, this.d * r);
   }

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) + 
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {
      if ((a*a + b*b + c*c + d*d) < DELTA){
         throw new ArithmeticException("Can not divide with 0:Real Part");
      }else{
         double newRealPart = a/(a*a + b*b + c*c + d*d);
         double newIPart = -b/(a*a + b*b + c*c + d*d);
         double newJPart = -c/(a*a + b*b + c*c + d*d);
         double newKPart = -d/(a*a + b*b + c*c + d*d);
         return new Quaternion(newRealPart, newIPart, newJPart, newKPart);
      }
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {
      return new Quaternion(getRpart() - q.getRpart(), getIpart() - q.getIpart(),
              getJpart() - q.getJpart(), getKpart() - q.getKpart());
   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {
      try {
         return this.times(q.inverse());
      }catch (ArithmeticException Exception){
         throw new ArithmeticException("Can not divide with zero");
      }
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {
      try {
         return q.inverse().times(this);
      }catch (ArithmeticException Exception){
         throw new ArithmeticException("Can not divide with zero");
      }
   }
   
   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {
      if (qo instanceof Quaternion){
         Quaternion newQuat = (Quaternion) qo;
         return getRpart() - newQuat.getRpart() < DELTA && getIpart() - newQuat.getIpart() < DELTA &&
                 getJpart() - newQuat.getJpart() < DELTA && getKpart() - newQuat.getKpart() < DELTA;
      }
      return false;
   }

   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */

   public Quaternion dotMult (Quaternion q) {
      return (times(q.conjugate()).plus(q.times(conjugate()))).divide(2);
   }

   public Quaternion divide (double r){
      return new Quaternion(this.a / r, this.b / r, this.c / r, this.d / r);
   }
   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      double[] quaternionValues = {a,b,c,d};
      int result = 17;
      for (Double item: quaternionValues) {
         result = 31 * result + item.hashCode();
      }
      return result;
   }

   /** Norm of the quaternion. Expressed by the formula 
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
      return Math.sqrt(this.a*this.a+this.b*this.b+this.c*this.c+this.d*this.d);
   }

   /** Main method for testing purposes. 
    * @param arg command line parameters
    */
   public static void main (String[] arg) {
      String f = "5+6j+7i+8k";
      System.out.println(f.toString());
      System.out.println(valueOf(f).toString());
      /*Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
      if (arg.length > 0)
         arv1 = valueOf (arg[0]);
      System.out.println ("first: " + arv1.toString());
      System.out.println ("real: " + arv1.getRpart());
      System.out.println ("imagi: " + arv1.getIpart());
      System.out.println ("imagj: " + arv1.getJpart());
      System.out.println ("imagk: " + arv1.getKpart());
      System.out.println ("isZero: " + arv1.isZero());
      System.out.println ("conjugate: " + arv1.conjugate());
      System.out.println ("opposite: " + arv1.opposite());
      System.out.println ("hashCode: " + arv1.hashCode());
      Quaternion res = null;
      try {
         res = (Quaternion)arv1.clone();
      } catch (CloneNotSupportedException e) {};
      System.out.println ("clone equals to original: " + res.equals (arv1));
      System.out.println ("clone is not the same object: " + (res!=arv1));
      System.out.println ("hashCode: " + res.hashCode());
      res = valueOf (arv1.toString());
      System.out.println ("string conversion equals to original: " 
         + res.equals (arv1));
      Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
      if (arg.length > 1)
         arv2 = valueOf (arg[1]);
      System.out.println ("second: " + arv2.toString());
      System.out.println ("hashCode: " + arv2.hashCode());
      System.out.println ("equals: " + arv1.equals (arv2));
      res = arv1.plus (arv2);
      System.out.println ("plus: " + res);
      System.out.println ("times: " + arv1.times (arv2));
      System.out.println ("minus: " + arv1.minus (arv2));
      double mm = arv1.norm();
      System.out.println ("norm: " + mm);
      System.out.println ("inverse: " + arv1.inverse());
      System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
      System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
      System.out.println ("dotMult: " + arv1.dotMult (arv2));*/
   }
}
// end of file
